package com.example.ficheLeroy;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class IdentityActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_identity);


        Intent mess = getIntent();
        String nom = mess.getStringExtra(MainActivity.NOM);
        String prenom = mess.getStringExtra(MainActivity.PRENOM);
        String tel = mess.getStringExtra(MainActivity.TEL);

        EditText nomView = findViewById(R.id.nomValue_identity);
        EditText prenomView = findViewById(R.id.prénomValue_identity);
        EditText telView = findViewById(R.id.telValue_identity);

        nomView.setText(nom);
        prenomView.setText(prenom);
        telView.setText(tel);

    }

    public void cancel(View v){
        Intent mess = new Intent();
        setResult(RESULT_CANCELED, mess);
        finish();
    }

    public void validate(View v){
        Intent mess = new Intent();
        EditText nomView = findViewById(R.id.nomValue_identity);
        EditText prenomView = findViewById(R.id.prénomValue_identity);
        EditText telView = findViewById(R.id.telValue_identity);

        mess.putExtra(MainActivity.NOM, nomView.getText().toString());
        mess.putExtra(MainActivity.PRENOM, prenomView.getText().toString());
        mess.putExtra(MainActivity.TEL, telView.getText().toString());

        setResult(RESULT_OK, mess);
        finish();
    }


}