package com.example.ficheLeroy;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    private ActivityResultLauncher<Intent> identityGetResult = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>(){
                @Override
                public void onActivityResult(ActivityResult result){
                    if (result.getResultCode() == RESULT_OK){
                        Intent mess = result.getData();
                        String nom = mess.getStringExtra(NOM);
                        String prenom = mess.getStringExtra(PRENOM);
                        String tel = mess.getStringExtra(TEL);

                        TextView nomView = findViewById(R.id.nomValue);
                        TextView prenomView = findViewById(R.id.prénomValue);
                        TextView telView = findViewById(R.id.telValue);

                        nomView.setText(nom);
                        prenomView.setText(prenom);
                        telView.setText(tel);

                    }
                }
            }

    );

    private ActivityResultLauncher<Intent> adresseGetResult = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>(){
                @Override
                public void onActivityResult(ActivityResult result){
                    if (result.getResultCode() == RESULT_OK){
                        Intent mess = result.getData();
                        String numero = mess.getStringExtra(NUMERO);
                        String rue = mess.getStringExtra(RUE);
                        String codePostal = mess.getStringExtra(CODE_POSTAL);
                        String ville = mess.getStringExtra(VILLE);

                        TextView numeroView = findViewById(R.id.numeroValue);
                        TextView rueView = findViewById(R.id.rueValue);
                        TextView codePostalView = findViewById(R.id.codePostalValue);
                        TextView villeView = findViewById(R.id.villeValue);

                        numeroView.setText(numero);
                        rueView.setText(rue);
                        codePostalView.setText(codePostal);
                        villeView.setText(ville);
                    }
                }
            }

    );



    public final static String NOM = "nom";
    public final static String PRENOM = "prenom";
    public final static String TEL = "tel";
    public void modifyIdentity(View v){
        Intent mess = new Intent(this, IdentityActivity.class);

        TextView nom = findViewById(R.id.nomValue);
        TextView prenom = findViewById(R.id.prénomValue);
        TextView tel = findViewById(R.id.telValue);
        mess.putExtra(NOM, nom.getText());
        mess.putExtra(PRENOM, prenom.getText());
        mess.putExtra(TEL, tel.getText());
        identityGetResult.launch(mess);
    }

    public final static String NUMERO = "numero";
    public final static String RUE = "rue";
    public final static String CODE_POSTAL = "codePostal";
    public final static String VILLE = "ville";
    public void modifyAdresse(View v){
        Intent mess = new Intent(this, AdresseActivity.class);
        TextView numero = findViewById(R.id.numeroValue);
        TextView rue = findViewById(R.id.rueValue);
        TextView code_postal = findViewById(R.id.codePostalValue);
        TextView ville = findViewById(R.id.villeValue);
        mess.putExtra(NUMERO, numero.getText());
        mess.putExtra(RUE, rue.getText());
        mess.putExtra(CODE_POSTAL, code_postal.getText());
        mess.putExtra(VILLE, ville.getText());
        adresseGetResult.launch(mess);
    }



}