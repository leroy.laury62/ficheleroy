package com.example.ficheLeroy;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class AdresseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adresse);

        Intent mess = getIntent();
        String numero = mess.getStringExtra(MainActivity.NUMERO);
        String rue = mess.getStringExtra(MainActivity.RUE);
        String codePostal = mess.getStringExtra(MainActivity.CODE_POSTAL);
        String ville = mess.getStringExtra(MainActivity.VILLE);

        EditText numeroView = findViewById(R.id.numeroValue_adresse);
        EditText rueView = findViewById(R.id.rueValue_adresse);
        EditText codePostalView = findViewById(R.id.codePostalValue_adresse);
        EditText villeView = findViewById(R.id.villeValue_adresse);


        numeroView.setText(numero);
        rueView.setText(rue);
        codePostalView.setText(codePostal);
        villeView.setText(ville);
    }

    public void cancel(View v){
        Intent mess = new Intent();
        setResult(RESULT_CANCELED, mess);
        finish();
    }

    public void validate(View v){
        Intent mess = new Intent();
        EditText numeroView = findViewById(R.id.numeroValue_adresse);
        EditText rueView = findViewById(R.id.rueValue_adresse);
        EditText codePostalView = findViewById(R.id.codePostalValue_adresse);
        EditText villeView = findViewById(R.id.villeValue_adresse);

        mess.putExtra(MainActivity.NUMERO, numeroView.getText().toString());
        mess.putExtra(MainActivity.RUE, rueView.getText().toString());
        mess.putExtra(MainActivity.CODE_POSTAL, codePostalView.getText().toString());
        mess.putExtra(MainActivity.VILLE, villeView.getText().toString());

        setResult(RESULT_OK, mess);
        finish();
    }
}